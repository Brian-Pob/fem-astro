/** @type {import('tailwindcss').Config} */
// - Violet: hsl(257, 40%, 49%)
// - Soft Magenta: hsl(300, 69%, 71%)
export default {
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
  theme: {
    extend: {
      colors: {
        brand: {
          DEFAULT: " hsl(257 40% 49%)",
          2: "hsl(300 69% 71%)",
        },
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
